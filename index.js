
// Use the "Require" directive to loas Node.js modules
	// A "module" is a software component or part of a program that contain one or more routines.
// "hhtp module" let's Node.js to transfer data using the Hypertext Transfer Protocol.
	// This is a set of individual files that contain code to crerate a "component" that helps establish data transger between appliction
	// allows us to fetch resources such as HTML documents.
// Clients (browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages (request/responses).
// The message sent by the client is called "request"
// The message sent by the server as an answer is called "response"
let http = require ("http");

// Using this module's "createServer()" method, we can create an HTTP server that listens to the request on a specified port and gives responses to the client.

// A port is a virtual point where network connections start and end.
// Each port is associated with specific process or services.
// The server will be assigned to port 4000 via the "listen()" mehtod where the server will listen to any request that are sent to it and will also sent the response on via this port.
http.createServer(function(request, response){
	// Use to the writeHead() method to:
		// set a status code for the response. (200 measns "OK")
		// Set the content-type of the response. (plain text message).
	response.writeHead(200, {"Content-Type": "text/plain"});

	response.end("Hello world");

	// response.writeHead(200, {"Content-Type": "text/html"});

	// response.end("<h1>Hello World</h1>");
}).listen(4000);

console.log("Server is running at localhost:4000");

// 3000, 4000, 8000, 5000 - Usually for web development.
// We will access the server in the browser using the localhost:4000